#include <iostream>


class Player
{
private:
	std::string name;
	int score;
public:
	void getDataPlayer();
	void getDataPlayers();
};

// ��� ��������� ������ ������ �� ������������
void Player::getDataPlayer()
{
	std::cout << "Enter name: ";
	std::cin >> name;
	std::cout << "Enter score: ";
	std::cin >> score;
}

// ��� ��������� ������ ������� � �������
void Player::getDataPlayers()
{
	int NumberOfPlayers;
	std::cout << "Enter number of Players: " << "\n";
	std::cin >> NumberOfPlayers;

	Player* players = new Player[NumberOfPlayers];

	for (int i = 0; i < NumberOfPlayers; i++)
		players[i].getDataPlayer();


	Player** p = new Player * [NumberOfPlayers];
	for (int i = 0; i < NumberOfPlayers; i++)
		p[i] = &players[i];

	// ��� ���������� �������
	for (int i = 0; i < NumberOfPlayers; i++)
		for (int j = 0; j < NumberOfPlayers - i - 1; j++)
			if (p[j]->score < p[j + 1]->score)
			{
				Player* temp = p[j];
				p[j] = p[j + 1];
				p[j + 1] = temp;
			}

	for (int i = 0; i < NumberOfPlayers; i++)
		std::cout << p[i]->name << " " << p[i]->score << "\n";

	delete[] players;
}



int main()
{
	Player plr;
	plr.getDataPlayers();

	return 0;
}